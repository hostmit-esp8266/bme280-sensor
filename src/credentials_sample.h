// RENAME FILE TO credentials.h, set variables.

#ifndef CREDENTIALS_H
#define CREDENTIALS_H

#define WIFI_DEFAULT_SSID "YOURWIFISSID"
#define WIFI_DEFAULT_PASS "YOURWIFIPASS"

#define BLYNK_HOST "blynk.hostname"
#define BLYNK_PORT 8090
#define BLYNK_TOKEN "token" //dev

#define GELF_UDP_LOGGER_HOSTNAME "graylog.hostname"
#define GELF_UDP_LOGGER_PORT 12201
#define GELF_UDP_LOGGER_MAX_LENGTH 512


#endif