#ifndef SENSOR_H
#define SENSOR_H

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <ESPAsyncUDP.h>

#include <BlynkSimpleEsp8266.h>

#include "config.h"
#include "logger.h"
#include "timetools.h"
#include "gelfudplogger.h"

Adafruit_BME280 bme;

class Sensor
{
public:
    static bool status;
    static float temperature;
    static float pressure;
    static float altitude;
    static float humidity;
    static ulong lastRead;
    static ulong lastResult;
    static AsyncUDP udp;

    static void setup()
    {
        if(udp.listenMulticast(IPAddress(239,1,1,2), SENSOR_LISTEN_PORT)) {
            log("Listening on port:" + String(SENSOR_LISTEN_PORT), LOG_INFO);
        }

        bme.begin(0x76);
    }
    static void handle()
    {
        if (millis() - lastRead > SENSOR_READ_DELAY)
        {
            lastRead = millis();
            temperature = bme.readTemperature();
            pressure = bme.readPressure() / 100.0F;
            altitude = bme.readAltitude(SENSOR_SEALEVELPRESSURE_HPA);
            humidity = bme.readHumidity();
            if (isnan(temperature) || temperature < SENSOR_THRESHOLD_LOWER || isnan(humidity))
            {
                if (status == true)
                    log("Sensor FAILED, will try to reconnect. Temperature readings:" + String(temperature), LOG_ERR);
                status = false;
                bme.begin(0x76);
            }
            else
            {
                temperature = temperature + SENSOR_CORRECTION; //correction
                if (status == false)
                {
                    log("Sensor connected, temp readings: " + String(temperature), LOG_INFO);
                    status = true;
                }
                udp.print(get_json().c_str());
                
                Blynk.virtualWrite(BLYNK_TEMPERATURE_VPIN, temperature);
                Blynk.setProperty(BLYNK_TEMPERATURE_VPIN, "label", String(SENSOR_NAME)+ "[" + Timetools::formattedDateTime() +"]");

                Blynk.virtualWrite(BLYNK_HUMIDITY_VPIN, humidity);
                Blynk.setProperty(BLYNK_HUMIDITY_VPIN, "label", String(SENSOR_NAME)+ "[" + Timetools::formattedDateTime() +"]");

                Blynk.virtualWrite(BLYNK_PRESSURE_VPIN, pressure);
                Blynk.setProperty(BLYNK_PRESSURE_VPIN, "label", String(SENSOR_NAME)+ "[" + Timetools::formattedDateTime() +"]");

                lastResult = millis();
            }
        }
    }

    static String get_json()
    {
        StaticJsonDocument<200> doc;
        doc["source"] = SENSOR_SOURCE;
        doc["name"] = SENSOR_NAME;
        doc["temperature"] = temperature;
        doc["humidity"] = humidity;
        doc["pressure"] = pressure;
        doc["altitude"] = altitude;
        String r;
        serializeJsonPretty(doc, r);
        return r;
    }

    static void log(String msg, uint level = 6)
    {
        if (level <= 6) Logger::syslog(msg);
        GelfUdpLogger::log(msg, level);
    }

    static String getWebInfo() {
        String s = "";
        s = "lastResult: " + Timetools::formattedDateTimeFromMillis(lastResult) + ", temperature: " + String(temperature)
             + ", pressure: " + String(pressure) + ", altitude: " + String(altitude) + ", humidity: " + String(humidity) + ", SENSOR_CORRECTION: " + String(SENSOR_CORRECTION);
        return s;
    }

};

bool Sensor::status = false;
float Sensor::temperature = -1000;
float Sensor::pressure = -1000;
float Sensor::altitude = -1000;
float Sensor::humidity = -1000;
ulong Sensor::lastRead = 0;
ulong Sensor::lastResult = 0;

AsyncUDP Sensor::udp;

#endif