#ifndef GELF_UDP_LOGGER_H
#define GELF_UDP_LOGGER_H

#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include "ESPAsyncUDP.h"
#include "config.h"
#include "credentials.h"


class GelfUdpLogger {

    public:

    static bool setupCompleted;
    static AsyncUDP udp;

    static void setup() {
        IPAddress gelfIp;
        int err = WiFi.hostByName(GELF_UDP_LOGGER_HOSTNAME,gelfIp);
        if (err == 1 ) {
            selfLog("GELF hostname: " + String(GELF_UDP_LOGGER_HOSTNAME) + ", resolved to: " + String(gelfIp.toString()));
            selfLog(gelfIp.toString());
            if(udp.connect(IPAddress(gelfIp), GELF_UDP_LOGGER_PORT)) {
                selfLog("Gelf UDP connected to " + gelfIp.toString() + ":" + String(GELF_UDP_LOGGER_PORT));
                setupCompleted = true;
            }
            
            
        } else {
            selfLog("Unable to resolve hostname: " + String(GELF_UDP_LOGGER_HOSTNAME));
        }
        
    }
    static void log (String msg, uint level = LOG_INFO) {
        if (msg.length() > GELF_UDP_LOGGER_MAX_LENGTH) msg.substring(0,GELF_UDP_LOGGER_MAX_LENGTH);
        StaticJsonDocument<GELF_UDP_LOGGER_MAX_LENGTH*2> doc;
        doc["host"] = GENERAL_SOURCE;
        doc["type"] = GENERAL_TYPE;
        doc["name"] = GENERAL_NAME;
        doc["version"] = "1.1";
        doc["short_message"] = msg;
        doc["level"] = level;
        String r;
        serializeJsonPretty(doc, r);
        udp.print(r.c_str());
    }

    static void selfLog(String msg, uint level = LOG_INFO) {
        Serial.println(msg);
    }
};
bool GelfUdpLogger::setupCompleted = false;
AsyncUDP GelfUdpLogger::udp;

#endif